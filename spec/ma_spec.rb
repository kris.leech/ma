RSpec.describe Ma do
  it "has a version number" do
    expect(Ma::VERSION).not_to be nil
  end

  describe '#subscriber' do
    it 'returns a Subscriber module' do
      expect(described_class.subscriber).to be_a(Ma::Subscriber)
    end

    it 'initalizes module with given arguments' do
      args = [:async]
      expect(Ma::Subscriber).to receive(:new).with(*args)
      described_class.subscriber(*args)
    end
  end

  describe '#publisher' do
    it 'returns a Publisher module' do
      expect(described_class.publisher).to be_a(Ma::Publisher)
    end
  end
end
