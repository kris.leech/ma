RSpec.describe Ma::Subscriber do
  describe '#on' do
    it 'creates a event method named after Event class' do
      stub_const('UserPromotedEvent', double(name: 'UserPromotedEvent'))

      subscriber = Class.new do
        include Ma::Subscriber.new

        on(UserPromotedEvent) {}
      end.new

      expect(subscriber).to respond_to(:on_UserPromotedEvent)
    end

    describe 'when class name is underscorable' do
      it 'creates an underscored method named after the Event class' do
        name = double(underscore: 'user_promoted_event')

        stub_const('UserPromotedEvent', double(name: name))

        subscriber = Class.new do
          include Ma::Subscriber.new

          on(UserPromotedEvent) {}
        end.new

        expect(subscriber).to respond_to(:on_user_promoted_event)
      end
    end

    it 'creates a event method named after Event class with artiy of 1' do
      class UserPromotedEvent
      end

      subscriber = Class.new do
        include Ma::Subscriber.new

        on(UserPromotedEvent) {}
      end.new

      expect(subscriber.method(:on_UserPromotedEvent).arity).to eq(1)
    end

    describe 'when :async option passed' do
      describe 'and async broadcaster is defined' do
        it 'uses async broadcaster' do
          async_broadcaster = double
          async_broadcaster_class = double(new: async_broadcaster)

          stub_const('UserPromotedEvent', double('UserPromotedEvent', new: double('user_promoted_event'), name: 'UserPromotedEvent'))
          stub_const('WisperNext::Subscriber::AsyncBroadcaster', async_broadcaster)

          expect(async_broadcaster).to receive(:new)

          subscriber = Class.new do
            include Ma::Subscriber.new(:async)

            on(UserPromotedEvent) {}
          end.new

          subscriber.send(:on_UserPromotedEvent, UserPromotedEvent.new)
        end
      end
    end
  end

  describe 'strict option' do
    describe 'when not given' do
      it 'is not strict' do
        expect(Ma::Subscriber.new.options.fetch(:strict)).to eq(false)
      end
    end

    describe 'when true' do
      it 'is strict' do
        strict = true
        expect(Ma::Subscriber.new(strict: strict).options.fetch(:strict)).to eq(strict)
      end
    end

    describe 'when false' do
      it 'is not strict' do
        strict = false
        expect(Ma::Subscriber.new(strict: strict).options.fetch(:strict)).to eq(strict)
      end
    end
  end
end
