require 'wisper_next'

module Ma
  class Subscriber < Module
    Prefix = true

    attr_reader :options

    def initialize(*args)
      @options = {
        strict: false,
        prefix: Prefix
      }.merge(WisperNext::CastToOptions.(args))
    end

    def included(base)
      base.extend(ClassMethods)
      base.include(WisperNext.subscriber(@options))

      super
    end

    module ClassMethods
      def on(event_class, &block)
        method_name = WisperNext::Subscriber::ResolveMethod.(event_class.name, Prefix)

        define_method method_name do |payload|
          instance_exec(event_class.new(payload), &block)
        end
      end
    end
  end
end
